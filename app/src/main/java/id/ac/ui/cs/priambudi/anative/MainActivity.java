package id.ac.ui.cs.priambudi.anative;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WifiConfiguration conf = new WifiConfiguration();

        final String networkSSID = "contohssid";
        final String networkPass = "contohpassword";

        conf.SSID = String.format("\"%s\"", networkSSID);
        conf.preSharedKey = String.format("\"%s\"", networkPass);

        final WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService (Context.WIFI_SERVICE);
        WifiInfo info = wifiManager.getConnectionInfo();
        final String ssid  = info.getSSID();
        final int netId = wifiManager.addNetwork(conf);
        // Example of a call to a native method
        TextView tv = findViewById(R.id.sample_text);
        int x = 2;
        int y = 2;
        tv.setText("SSID: "+ssid+" , addition result of "+x+" and "+y+" is: "+stringFromJNI(x,y));

        ConnectivityManager.NetworkCallback networkCallback = new ConnectivityManager
                .NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                Toast.makeText(getApplication(), "Was connected to wifi! SSID: "+ssid,
                        Toast.LENGTH_LONG).show();
                if (!ssid.equals(networkSSID) && !ssid.equals("<unknown ssid>")){
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplication(), "Disconnecting WIFI",
                                    Toast.LENGTH_LONG).show();
                            wifiManager.disconnect();
                        }
                    }, 3000);
                }
            }

            @Override
            public void onLost(Network network) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplication(), "Connecting to "+networkSSID+" with password: "+networkPass,
                                Toast.LENGTH_LONG).show();
                        wifiManager.enableNetwork(netId, true);
                        wifiManager.reconnect();
                    }
                }, 5000);
            }
        };

        ConnectivityManager connectivityManager =
                (ConnectivityManager) Objects.requireNonNull(getApplicationContext())
                        .getSystemService(Context.CONNECTIVITY_SERVICE);
        connectivityManager.registerDefaultNetworkCallback(networkCallback);
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native int stringFromJNI(int x, int y);
}
